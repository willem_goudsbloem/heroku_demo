var http = require('http');
var express = require('express');
var app = express();
var server = http.createServer(app);

var redis = require('redis');
var url = require('url');
var redisURL = url.parse(process.env.REDISCLOUD_URL);
var client = redis.createClient(redisURL.port, redisURL.hostname, {no_ready_check: true});
client.auth(redisURL.auth.split(":")[1]);

app.set('view engine', 'jade');

app.get('/', function(req, res){
  var year = new Date().getFullYear();
  res.render('index', {'year':year});
});

app.get('/message', function(req, res){
  console.log(req.query.msg);
  client.set('msg', req.query.msg);
  res.status(200).send('received');
});

app.get('/message/retrieve', function(req, res){
  client.get('msg', function(err, value){
    res.status(200).send('your message is still ' + value.toString());
  });
});

server.listen(process.env.PORT || 8080);

console.log('server running on port %d', server.address().port);
